import logo from '../assets/logo.png';

function Header(){
    const listHeader=['Lợi ích','Lĩnh vực','Cuộc hẹn','Hướng dẫn','Q&A','Review','Góp ý']
    return (
        <div className='header'>
            <img src={logo} alt=''/>
            <ul>
                {listHeader.map((item,index)=>{
                    return (
                        <li key={index}>
                            <a href='#'>{item}</a>
                        </li>
                    )
                })
                }
            </ul>
        </div>
    )
}
export default Header