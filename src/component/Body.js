import page1 from '../assets/page1.png'
import page2 from '../assets/page2.png'
import Header from './Header'
import Footer from './Footer'
import './style.css'
function Body(){
    return (
        <div className='body'>
            <Header />
            <img src={page1} alt=''/>
            <img src={page2} alt=''/>
            <Footer/>
        </div>
    )
}
export default Body